﻿#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>

class FileReader {
private:
    std::vector<char> allBytes;
    std::vector<std::vector<std::vector<char>>> allLines;

    double getNumeric(std::vector<std::vector<char>>& line, int number) {
        std::string numberBuilder = "";
        int numberLength = line.at(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)line.at(number).at(digit);
            numberBuilder += symbol;
        }

        double numeric;
        std::stringstream(numberBuilder) >> numeric;
        return numeric;
    }

    auto analyzeSyntaxAndReturnLinesList() {
        std::vector<std::vector<std::vector<char>>> allLines;
        auto words = new std::vector<std::vector<char>>;
        auto symbols = new std::vector<char>;
        bool isWord = false;

        char tab = 9;
        char newLine = 10;
        char carriageReturn = 13;
        char space = 32;

        for (char symbol : allBytes) {
            if (symbol == newLine) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                allLines.push_back(*words);
                delete words;
                words = new std::vector<std::vector<char>>;
                isWord = false;
            } else if ((isWord) && (symbol == tab)) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                isWord = false;
            } else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols->push_back(symbol);
            }
        }
        delete symbols;
        delete words;
        return allLines;
    }

public:
    FileReader(std::string fileName) {
        std::ifstream in(fileName);
        char byte;
        if (in.is_open()) {
            while (in.get(byte)) {
                allBytes.push_back(byte);
            }
        } else {
            exit(1);
        }
        in.close();
        allLines = analyzeSyntaxAndReturnLinesList();
    }

    auto getMeasurements(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        std::vector<std::vector<double>> measurements;
        int numbersInLine = 21;

        for (auto line : allLines) {
            try {
                char satelliteNumber1 = line.at(0).at(0);
                char satelliteNumber2 = line.at(0).at(1);
                int satelliteNumberSize = line.at(0).size();

                if ((satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    std::vector<double> lineOfNumbers;
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.push_back(numeric);
                    }
                    measurements.push_back(lineOfNumbers);
                }
            }
            catch (std::exception ignored) {}
        }
        return measurements;
    }

    auto getMeasurements(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        std::vector<std::vector<double>> measurements;
        int numbersInLine = 21;

        for (auto line : allLines) {
            try {
                char satelliteNumber = line.at(0).at(0);
                int satelliteNumberSize = line.at(0).size();

                if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    std::vector<double> lineOfNumbers;
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.push_back(numeric);
                    }
                    measurements.push_back(lineOfNumbers);
                }
            }
            catch (std::exception ignored) {}
        }
        return measurements;
    }
};

class DelayComponent {
public:
    virtual std::vector<double> getValues() = 0;
};

class DryComponentFunction : public DelayComponent {
public:
    DryComponentFunction(std::vector<double> values) {
        this->values = values;
    }

    virtual std::vector<double> getValues() override {
        return values;
    }

private:
    std::vector<double> values;
};

class VerticalDryComponent : public DelayComponent {
public:
    VerticalDryComponent(std::vector<double> values) {
        this->values = values;
    }

    virtual std::vector<double> getValues() override {
        return values;
    }

private:
    std::vector<double> values;
};

class WetComponentFunction : public DelayComponent {
public:
    WetComponentFunction(std::vector<double> values) {
        this->values = values;
    }

    virtual std::vector<double> getValues() override {
        return values;
    }

private:
    std::vector<double> values;
};

class VerticalWetComponent : public DelayComponent {
public:
    VerticalWetComponent(std::vector<double> values) {
        this->values = values;
    }

    virtual std::vector<double> getValues() override {
        return values;
    }

private:
    std::vector<double> values;
};

class TroposphericDelays {
public:
    TroposphericDelays(DelayComponent& dryComponentFunction, DelayComponent& verticalDryComponent,
                       DelayComponent& wetComponentFunction, DelayComponent& verticalWetComponent,
                       int amountOfObservations) {
        this->dryComponentFunction = &dryComponentFunction;
        this->verticalDryComponent = &verticalDryComponent;
        this->wetComponentFunction = &wetComponentFunction;
        this->verticalWetComponent = &verticalWetComponent;
        this->amountOfObservations = amountOfObservations;
    }

    std::vector<double> getDelays() {
        std::vector<double> dryComponentFunctionValues = dryComponentFunction->getValues();
        std::vector<double> verticalDryComponentValues = verticalDryComponent->getValues();
        std::vector<double> wetComponentFunctionValues = wetComponentFunction->getValues();
        std::vector<double> verticalWetComponentValues = verticalWetComponent->getValues();

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double md = dryComponentFunctionValues.at(observation);
            double td = verticalDryComponentValues.at(observation);
            double mw = wetComponentFunctionValues.at(observation);
            double tw = verticalWetComponentValues.at(observation);
            double delay = md * td + mw * tw;
            delays.push_back(delay);
        }
        return delays;
    }

private:
    DelayComponent* dryComponentFunction;
    DelayComponent* verticalDryComponent;
    DelayComponent* wetComponentFunction;
    DelayComponent* verticalWetComponent;
    int amountOfObservations;
    std::vector<double> delays;
};

class ElevationAngles {
public:
    ElevationAngles(std::vector<double> degrees) {
        this->degrees = degrees;
    }

    std::vector<double> getAnglesInHalfCircles() {
        double halfCircle = 180;
        std::vector<double> halfCircles;
        for (double angleInDegrees : degrees) {
            double angleInHalfCircles = angleInDegrees / halfCircle;
            halfCircles.push_back(angleInHalfCircles);
        }
        return halfCircles;
    }

private:
    std::vector<double> degrees;
};

class Satellite {
public:
    Satellite(int number, TroposphericDelays& troposphericDelays, ElevationAngles& elevationAngles) {
        this->number = number;
        this->troposphericDelays = &troposphericDelays;
        this->elevationAngles = &elevationAngles;
    }

    int getNumber() {
        return number;
    }

    std::vector<double> getTroposphericDelays() {
        std::vector<double> delays = troposphericDelays->getDelays();
        return delays;
    }

    std::vector<double> getElevationAngles() {
        std::vector<double> angles = elevationAngles->getAnglesInHalfCircles();
        return angles;
    }

private:
    int number;
    TroposphericDelays* troposphericDelays;
    ElevationAngles* elevationAngles;
};

class SatelliteFactory {
public:
    SatelliteFactory(FileReader& fileReader, int amountOfObservations) {
        this->fileReader = &fileReader;
        this->amountOfObservations = amountOfObservations;
    }

    Satellite* createSatellite(char satelliteNumber1, char satelliteNumber2, int satelliteNumberSize) {
        std::string satelliteNumber = std::string(1, satelliteNumber1) + satelliteNumber2;
        int satelliteNumeric = std::stoi(satelliteNumber);
        std::vector<double> mdArray;
        std::vector<double> tdArray;
        std::vector<double> mwArray;
        std::vector<double> twArray;
        std::vector<double> elevationArray;
        auto measurements = fileReader->getMeasurements(satelliteNumber1, satelliteNumber2, satelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double md = measurements.at(observation).at(5);
            mdArray.push_back(md);
            double td = measurements.at(observation).at(6);
            tdArray.push_back(td);
            double mw = measurements.at(observation).at(7);
            mwArray.push_back(mw);
            double tw = measurements.at(observation).at(8);
            twArray.push_back(tw);
            double elevation = measurements.at(observation).at(14);
            elevationArray.push_back(elevation);
        }
        DelayComponent* dryComponentFunction = new DryComponentFunction(mdArray);
        DelayComponent* verticalDryComponent = new VerticalDryComponent(tdArray);
        DelayComponent* wetComponentFunction = new WetComponentFunction(mwArray);
        DelayComponent* verticalWetComponent = new VerticalWetComponent(twArray);
        TroposphericDelays* delays = new TroposphericDelays(*dryComponentFunction, *verticalDryComponent,
                                                            *wetComponentFunction, *verticalWetComponent,
                                                            amountOfObservations);
        ElevationAngles* angles = new ElevationAngles(elevationArray);
        Satellite* satellite = new Satellite(satelliteNumeric, *delays, *angles);
        return satellite;
    }

    Satellite* createSatellite(char satelliteNumber, int satelliteNumberSize) {
        int satelliteNumeric = asciiToSymbol(satelliteNumber);
        std::vector<double> mdArray;
        std::vector<double> tdArray;
        std::vector<double> mwArray;
        std::vector<double> twArray;
        std::vector<double> elevationArray;
        auto measurements = fileReader->getMeasurements(satelliteNumber, satelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double md = measurements.at(observation).at(5);
            mdArray.push_back(md);
            double td = measurements.at(observation).at(6);
            tdArray.push_back(td);
            double mw = measurements.at(observation).at(7);
            mwArray.push_back(mw);
            double tw = measurements.at(observation).at(8);
            twArray.push_back(tw);
            double elevation = measurements.at(observation).at(14);
            elevationArray.push_back(elevation);
        }
        DelayComponent* dryComponentFunction = new DryComponentFunction(mdArray);
        DelayComponent* verticalDryComponent = new VerticalDryComponent(tdArray);
        DelayComponent* wetComponentFunction = new WetComponentFunction(mwArray);
        DelayComponent* verticalWetComponent = new VerticalWetComponent(twArray);
        TroposphericDelays* delays = new TroposphericDelays(*dryComponentFunction, *verticalDryComponent,
                                                            *wetComponentFunction, *verticalWetComponent,
                                                            amountOfObservations);
        ElevationAngles* angles = new ElevationAngles(elevationArray);
        Satellite* satellite = new Satellite(satelliteNumeric, *delays, *angles);
        return satellite;
    }

private:
    FileReader* fileReader;
    int amountOfObservations;
    int asciiToSymbol(char asciiCode) {
        return asciiCode - 48;
    }
};

class ConsoleOutput {
public:
    ConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
        this->amountOfObservations = amountOfObservations;
    }

    void printInfo() {
        int satellite1Number = satellite1->getNumber();
        int satellite2Number = satellite2->getNumber();
        int satellite3Number = satellite3->getNumber();
        std::vector<double> satellite1TroposphericDelays = satellite1->getTroposphericDelays();
        std::vector<double> satellite2TroposphericDelays = satellite2->getTroposphericDelays();
        std::vector<double> satellite3TroposphericDelays = satellite3->getTroposphericDelays();
        std::vector<double> satellite1ElevationAngles = satellite1->getElevationAngles();
        std::vector<double> satellite2ElevationAngles = satellite2->getElevationAngles();
        std::vector<double> satellite3ElevationAngles = satellite3->getElevationAngles();
        std::cout << "Спутник #" << satellite1Number <<
                     "\tСпутник #" << satellite2Number <<
                     "\tСпутник #" << satellite3Number << std::endl;
        std::cout << std::fixed << std::setprecision(10);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1TroposphericDelay = satellite1TroposphericDelays.at(observation);
            double satellite1ElevationAngle = satellite1ElevationAngles.at(observation);
            double satellite2TroposphericDelay = satellite2TroposphericDelays.at(observation);
            double satellite2ElevationAngle = satellite2ElevationAngles.at(observation);
            double satellite3TroposphericDelay = satellite3TroposphericDelays.at(observation);
            double satellite3ElevationAngle = satellite3ElevationAngles.at(observation);
            std::cout << satellite1TroposphericDelay << "\t" << satellite1ElevationAngle << "\t\t" <<
                         satellite2TroposphericDelay << "\t" << satellite2ElevationAngle << "\t\t" <<
                         satellite3TroposphericDelay << "\t" << satellite3ElevationAngle << std::endl;
        }
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
    int amountOfObservations;
};


int main() {
    setlocale(LC_ALL, "rus");
    int amountOfObservations = 360;

    char satellite1Number = '3';
    int satellite1NumberSize = 1;

    char satellite2Number = '6';
    int satellite2NumberSize = 1;

    char satellite3Number = '9';
    int satellite3NumberSize = 1;

    FileReader* fileReader = new FileReader("C:\\Users\\GVOZDEV\\Desktop\\cppPort\\resources\\arti_6hours.dat");
    SatelliteFactory* satelliteFactory = new SatelliteFactory(*fileReader, amountOfObservations);

    Satellite* satellite1 = satelliteFactory->createSatellite(satellite1Number, satellite1NumberSize);
    Satellite* satellite2 = satelliteFactory->createSatellite(satellite2Number, satellite2NumberSize);
    Satellite* satellite3 = satelliteFactory->createSatellite(satellite3Number, satellite3NumberSize);

    ConsoleOutput consoleOutput(*satellite1, *satellite2, *satellite3, amountOfObservations);
    consoleOutput.printInfo();
}