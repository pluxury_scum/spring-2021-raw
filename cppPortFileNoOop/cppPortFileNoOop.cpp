﻿#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <iomanip>

auto readBytes(std::string fileName) {
	std::ifstream in(fileName);
	std::vector<char> allBytes;
	char byte;
	if (in.is_open()) {
		while (in.get(byte)) {
			allBytes.push_back(byte);
		}
	} else {
		exit(1);
	}
	in.close();
	return allBytes;
}

auto analyzeSyntaxAndReturnLinesList(std::vector<char> allBytes) {
	auto allLines = new std::vector<std::vector<std::vector<char>>>;
	auto words = new std::vector<std::vector<char>>;
	auto symbols = new std::vector<char>;
	bool isWord = false;

	char tab = 9;
	char newLine = 10;
	char carriageReturn = 13;
	char space = 32;

	for (char symbol : allBytes) {
		if (symbol == newLine) {
			words->push_back(*symbols);
			delete symbols;
			symbols = new std::vector<char>;
			allLines->push_back(*words);
			delete words;
			words = new std::vector<std::vector<char>>;
			isWord = false;
		} else if ((isWord) && (symbol == tab)) {
			words->push_back(*symbols);
			delete symbols;
			symbols = new std::vector<char>;
			isWord = false;
		} else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
			isWord = true;
			symbols->push_back(symbol);
		}
	}
	delete symbols;
	delete words;
	return allLines;
}

double __getNumeric(std::vector<std::vector<char>>& line, int number) {
	std::string numberBuilder = "";
	int numberLength = line.at(number).size();

	for (int digit = 0; digit < numberLength; digit++) {
		char symbol = (char)line.at(number).at(digit);
		numberBuilder += symbol;
	}

	double numeric;
	std::stringstream(numberBuilder) >> numeric;
	return numeric;
}

auto __getMeasurements(std::vector<std::vector<std::vector<char>>>* allLines, char requiredSatelliteNumber1, char requiredSatelliteNumber2,
					   int requiredSatelliteNumberSize) {
	std::vector<std::vector<double>> measurements;
	int numbersInLine = 21;

	for (auto line : *allLines) {
		try {
			char satelliteNumber1 = line.at(0).at(0);
			char satelliteNumber2 = line.at(0).at(1);
			int satelliteNumberSize = line.at(0).size();

			if ((satelliteNumber1 == requiredSatelliteNumber1) &&
				(satelliteNumber2 == requiredSatelliteNumber2) &&
				(satelliteNumberSize == requiredSatelliteNumberSize)) {
				std::vector<double> lineOfNumbers;
				for (int number = 1; number <= numbersInLine; number++) {
					double numeric = __getNumeric(line, number);
					lineOfNumbers.push_back(numeric);
				}
				measurements.push_back(lineOfNumbers);
			}
		}
		catch (std::exception ignored) {}
	}
	return measurements;
}

auto __getMeasurements(std::vector<std::vector<std::vector<char>>>* allLines, char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
	std::vector<std::vector<double>> measurements;
	int numbersInLine = 21;

	for (auto line : *allLines) {
		try {
			char satelliteNumber = line.at(0).at(0);
			int satelliteNumberSize = line.at(0).size();

			if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
				std::vector<double> lineOfNumbers;
				for (int number = 1; number <= numbersInLine; number++) {
					double numeric = __getNumeric(line, number);
					lineOfNumbers.push_back(numeric);
				}
				measurements.push_back(lineOfNumbers);
			}
		}
		catch (std::exception ignored) {}
	}
	return measurements;
}

auto getTroposphericDelays(std::vector<std::vector<std::vector<char>>>* allLines, int amountOfObservations, char requiredSatelliteNumber1,
			   char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
	auto measurements = __getMeasurements(allLines, requiredSatelliteNumber1, requiredSatelliteNumber2, requiredSatelliteNumberSize);
	std::vector<double> delays;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double md = measurements.at(observation).at(5);
		double td = measurements.at(observation).at(6);
		double mw = measurements.at(observation).at(7);
		double tw = measurements.at(observation).at(8);
		double delay = md * td + mw * tw;
		delays.push_back(delay);
	}
	return delays;
}

auto getTroposphericDelays(std::vector<std::vector<std::vector<char>>>* allLines, int amountOfObservations, char requiredSatelliteNumber,
			   int requiredSatelliteNumberSize) {
	auto measurements = __getMeasurements(allLines, requiredSatelliteNumber, requiredSatelliteNumberSize);
	std::vector<double> delays;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double md = measurements.at(observation).at(5);
		double td = measurements.at(observation).at(6);
		double mw = measurements.at(observation).at(7);
		double tw = measurements.at(observation).at(8);
		double delay = md * td + mw * tw;
		delays.push_back(delay);
	}
	return delays;
}

auto getAnglesInHalfCircles(std::vector<std::vector<std::vector<char>>>* allLines, int amountOfObservations, char requiredSatelliteNumber1,
					char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
	auto measurements = __getMeasurements(allLines, requiredSatelliteNumber1, requiredSatelliteNumber2, requiredSatelliteNumberSize);
	double halfCircle = 180;
	std::vector<double> halfCircles;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double angleInDegrees = measurements.at(observation).at(14);
		double angleInHalfCircles = angleInDegrees / halfCircle;
		halfCircles.push_back(angleInHalfCircles);
	}
	return halfCircles;
}

auto getAnglesInHalfCircles(std::vector<std::vector<std::vector<char>>>* allLines, int amountOfObservations, char requiredSatelliteNumber,
					int requiredSatelliteNumberSize) {
	auto measurements = __getMeasurements(allLines, requiredSatelliteNumber, requiredSatelliteNumberSize);
	double halfCircle = 180;
	std::vector<double> halfCircles;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double angleInDegrees = measurements.at(observation).at(14);
		double angleInHalfCircles = angleInDegrees / halfCircle;
		halfCircles.push_back(angleInHalfCircles);
	}
	return halfCircles;
}

void printInfo(int satellite1Number, int satellite2Number, int satellite3Number,
			   std::vector<double> satellite1Delays, std::vector<double> satellite2Delays, std::vector<double> satellite3Delays,
			   std::vector<double> satellite1ElevationAngles, std::vector<double> satellite2ElevationAngles, std::vector<double> satellite3ElevationAngles,
			   int amountOfObservations) {
	std::cout << "Спутник #" << satellite1Number <<
				 "\tСпутник #" << satellite2Number <<
				 "\tСпутник #" << satellite3Number << std::endl;
	std::cout << std::fixed << std::setprecision(10);
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double satellite1TroposphericDelay = satellite1Delays.at(observation);
		double satellite1ElevationAngle = satellite1ElevationAngles.at(observation);
		double satellite2TroposphericDelay = satellite2Delays.at(observation);
		double satellite2ElevationAngle = satellite2ElevationAngles.at(observation);
		double satellite3TroposphericDelay = satellite3Delays.at(observation);
		double satellite3ElevationAngle = satellite3ElevationAngles.at(observation);
		std::cout << satellite1TroposphericDelay << "\t" << satellite1ElevationAngle << "\t\t" <<
					 satellite2TroposphericDelay << "\t" << satellite2ElevationAngle << "\t\t" <<
					 satellite3TroposphericDelay << "\t" << satellite3ElevationAngle << std::endl;
	}
}

int main() {
	int amountOfObservations = 360;

	char satellite1CharNumber = '3';
	int satellite1NumericNumber = 3;
	int satellite1NumberSize = 1;

	char satellite2CharNumber = '6';
	int satellite2NumericNumber = 6;
	int satellite2NumberSize = 1;

	char satellite3CharNumber = '9';
	int satellite3NumericNumber = 9;
	int satellite3NumberSize = 1;

	auto allBytes = readBytes("C:\\Users\\GVOZDEV\\Desktop\\cppPortFileNoOop\\resources\\arti_6hours.dat");
	auto allLines = analyzeSyntaxAndReturnLinesList(allBytes);

	auto satellite1Delays = getTroposphericDelays(allLines, amountOfObservations, satellite1CharNumber, satellite1NumberSize);
	auto satellite2Delays = getTroposphericDelays(allLines, amountOfObservations, satellite2CharNumber, satellite2NumberSize);
	auto satellite3Delays = getTroposphericDelays(allLines, amountOfObservations, satellite3CharNumber, satellite3NumberSize);

	auto satellite1ElevationAngles = getAnglesInHalfCircles(allLines, amountOfObservations, satellite1CharNumber, satellite1NumberSize);
	auto satellite2ElevationAngles = getAnglesInHalfCircles(allLines, amountOfObservations, satellite2CharNumber, satellite2NumberSize);
	auto satellite3ElevationAngles = getAnglesInHalfCircles(allLines, amountOfObservations, satellite3CharNumber, satellite3NumberSize);

	printInfo(satellite1NumericNumber, satellite2NumericNumber, satellite3NumericNumber,
			  satellite1Delays, satellite2Delays, satellite3Delays,
			  satellite1ElevationAngles, satellite2ElevationAngles, satellite3ElevationAngles,
			  amountOfObservations);
}
